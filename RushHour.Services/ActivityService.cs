﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using RushHour.Data.Entities;
using RushHour.Data.Repositories;

namespace RushHour.Services
{
    public class ActivityService : BaseService<Activity, ActivityRepository, UnitOfWork>
    {
        protected override UnitOfWork UnitOfWork { get; set; }
        protected override ActivityRepository Repository { get; set; }

        public ActivityService(IValidationDictionary validationDictionary) : base(validationDictionary)
        {
            UnitOfWork = new UnitOfWork();
            Repository = UnitOfWork.ActivityRepository;
        }

        protected override bool PreValidate(Activity activityToValidate)
        {
            if (string.IsNullOrEmpty(activityToValidate.Name))
            {
                this.validationDictionary.AddError("Name", "Name is required");
            }

            if (activityToValidate.Price < 0)
            {
                this.validationDictionary.AddError("Price", "Price cannot be less than zero");
            }

            if (activityToValidate.Duration <= 0)
            {
                this.validationDictionary.AddError("Duration", "Duration cannot be equal or less than zero");
            }

            return this.validationDictionary.IsValid;
        }
    }
}
