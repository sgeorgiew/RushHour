﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RushHour.Data.Repositories;
using RushHour.Data.Entities;

namespace RushHour.Services
{
    public class UserService : BaseService<User, UserRepository, UnitOfWork>
    {
        protected override UnitOfWork UnitOfWork { get; set; }
        protected override UserRepository Repository { get; set; }

        public UserService(IValidationDictionary validationDictionary) : base(validationDictionary)
        {
            UnitOfWork = new UnitOfWork();
            Repository = UnitOfWork.UserRepository;
        }

        protected override bool PreValidate(User userToValidate)
        {
            User dbUser = Repository.GetAll(u => u.Email == userToValidate.Email).FirstOrDefault();

            if (dbUser != null)
            {
                if (dbUser.Email == userToValidate.Email)
                {
                    this.validationDictionary.AddError("Email", "This email is already taken!");
                }
            }

            return this.validationDictionary.IsValid;
        }

        public bool ValidateDbUser(User user)
        {
            if (user != null)
            {
                return true;
            }

            return false;
        }

        public User GetByEmailAndPassword(string email, string password)
        {
            return Repository.GetByEmailAndPassword(email, password);
        }
    }
}
