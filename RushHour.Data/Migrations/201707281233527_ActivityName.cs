namespace RushHour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ActivityName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Activities", "Name", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Activities", "Name");
        }
    }
}
