namespace RushHour.Data.Migrations
{
    using RushHour.Data.Entities;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<RushHour.Data.RushHourContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RushHour.Data.RushHourContext context)
        {
            var users = new List<User>
            {
                new User
                {
                    Email = "admin@site.com",
                    Password = "FCDF97314235E66EAF7056D7FBB42204C5B54A3B62345253DF691C9B6CD030D3",
                    PasswordSalt = "3ac4f3f8-d344-434e-bb1f-4e6c6eb9efda",
                    Name = "Admin",
                    Phone = "2222222222",
                    IsAdministrator = true
                }
            };

            users.ForEach(u => context.Users.AddOrUpdate(u));
            context.SaveChanges();
        }
    }
}
