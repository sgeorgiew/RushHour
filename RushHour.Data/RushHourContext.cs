﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using RushHour.Data.Entities;

namespace RushHour.Data
{
    public class RushHourContext : DbContext
    {
        public RushHourContext() : base("RushHourDB")
        {
            Database.SetInitializer<RushHourContext>(new CreateDatabaseIfNotExists<RushHourContext>());
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Activity> Activities { get; set; }
        public DbSet<Appointment> Appointments { get; set; }
    }
}
