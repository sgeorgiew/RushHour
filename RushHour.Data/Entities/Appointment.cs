﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.Data.Entities
{
    public class Appointment : BaseEntity
    {
        [Required]
        public int UserId { get; set; }

        [Required]
        public DateTime StartDateTime { get; set; }

        [Required]
        public DateTime EndDateTime { get; set; }

        [Required]
        public bool IsCancelled { get; set; }

        public virtual User User { get; set; }

        public virtual ICollection<Activity> Activities { get; set; }
    }
}