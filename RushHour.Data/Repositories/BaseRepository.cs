﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using RushHour.Data.Entities;

namespace RushHour.Data.Repositories
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        private RushHourContext context;

        public BaseRepository() : this(new RushHourContext())
        { }

        public BaseRepository(RushHourContext context)
        {
            this.context = context;
        }

        public T GetById(int id)
        {
            return context.Set<T>().Find(id);
        }

        public IQueryable<T> GetAll(Func<T, bool> filter = null)
        {
            if (filter != null)
            {
                return context.Set<T>().Where(filter).AsQueryable();
            }
            
            return context.Set<T>().AsQueryable();
        }

        public void AddOrUpdate(T item)
        {
            if (item.Id <= 0)
            {
                Create(item);
            }
            else
            {
                Update(item);
            }
        }

        public bool DeleteById(int id)
        {
            T dbItem = context.Set<T>().Find(id);

            if (dbItem != null)
            {
                context.Set<T>().Remove(dbItem);
                return true;
            }

            return false;
        }
 
        protected virtual void Create(T item)
        {
            context.Set<T>().Add(item);
        }
        
        protected virtual void Update(T item)
        {
            context.Entry(item).State = EntityState.Modified;
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
