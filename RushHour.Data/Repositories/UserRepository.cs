﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RushHour.Data.Entities;
using RushHour.Data.Helpers;

namespace RushHour.Data.Repositories
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(RushHourContext context) : base(context)
        { }

        protected override void Create(User user)
        {
            HashUserPassword(user);
            base.Create(user);
        }

        protected override void Update(User user)
        {
            var userPassword = new UnitOfWork().UserRepository.GetById(user.Id).Password;

            if (user.Password != userPassword)
            {
                HashUserPassword(user);
            }

            base.Update(user);
        }

        public User GetByEmailAndPassword(string email, string password)
        {
            User user = base.GetAll().Where(u => u.Email == email).FirstOrDefault();

            if (user != null)
            {
                PasswordManager passwordManager = new PasswordManager();
                bool isValidPassword = passwordManager.IsPasswordMatch(password, user.Password, user.PasswordSalt);

                if (!isValidPassword)
                {
                    user = null;
                }
            }

            return user;
        }

        private void HashUserPassword(User user)
        {
            PasswordManager passwordManager = new PasswordManager();

            string hash = passwordManager.GeneratePasswordHash(user.Password, out string salt);
            user.Password = hash;
            user.PasswordSalt = salt;
        }
    }
}
