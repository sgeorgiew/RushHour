﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace RushHour.Helpers
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class CustomAuthorizeAttribute : FilterAttribute, IAuthorizationFilter
    {
        /// <summary>
        /// If you want to explicitly allow for a common user to access an action, set this to true
        /// </summary>
        public bool AllowAccessToUser { get; set; }

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            // if the Action or its Controller has [AllowAnonymous] attribute - do not perform validation
            if (filterContext.ActionDescriptor.GetCustomAttributes(typeof(AllowAnonymousAttribute), true).Any()
                || filterContext.ActionDescriptor.ControllerDescriptor.GetCustomAttributes(typeof(AllowAnonymousAttribute), true).Any())
            {
                return;
            }

            // check if the current session is not authenticated - then redirect to login
            if (!AuthenticationService.Current.IsAuthenticated)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary(new { controller = "User", action = "Login" }));
            }
            // then check if the current user should be able to access the action
            else if (!AllowAccessToUser && !AuthenticationService.Current.IsAdministrator)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary(new { controller = "Home", action = "Index" }));
            }
        }
    }
}