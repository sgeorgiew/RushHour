﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RushHour.Helpers
{
    /// <summary>
    /// Represents the main information for the logged user.
    /// We keep it in the session.
    /// To get the instance in the current session, we have to use AuthenticationService.Current, which is always not null
    /// </summary>
    public class AuthenticationService
    {
        #region Properties
        public int UserId { get; private set; }

        public string Email { get; private set; }
        
        public bool IsAuthenticated { get; private set; }
        
        public bool IsAdministrator { get; private set; }
        #endregion

        #region Public Properties
        public static AuthenticationService Current
        {
            get
            {
                AuthenticationService loginUserSession = (AuthenticationService)HttpContext.Current.Session["LoggedUser"];
                if (loginUserSession == null)
                {
                    loginUserSession = new AuthenticationService();
                    HttpContext.Current.Session["LoggedUser"] = loginUserSession;
                }

                return loginUserSession;
            }
        }
        #endregion

        #region Public Methods
        public void Authenticate(int userID, string email, bool isAdmin)
        {
            this.IsAuthenticated = true;
            this.UserId = userID;
            this.Email = email;
            this.IsAdministrator = isAdmin;
        }

        public void Logout()
        {
            this.IsAuthenticated = false;
            this.UserId = 0;
            this.Email = string.Empty;
            this.IsAdministrator = false;
        }
        #endregion
    }
}