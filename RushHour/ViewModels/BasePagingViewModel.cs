﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RushHour.Data.Entities;

namespace RushHour.ViewModels
{
    public class BasePagingViewModel<T> where T : class
    {
        public List<T> Items;

        #region Properties
        public const int ItemsPerPage = 10;

        public int CurrentPageIndex { get; set; }
        public int TotalPages { get; set; }
        public int TotalItems { get; set; }

        public bool HasFirstPage
        {
            get { return CurrentPageIndex > 1; }
        }

        public bool HasLastPage
        {
            get { return CurrentPageIndex < TotalPages; }
        }

        public bool HasPrevPage
        {
            get { return CurrentPageIndex > 1; }
        }

        public bool HasNextPage
        {
            get { return CurrentPageIndex < TotalPages; }
        }

        public bool IsCurrentPage(int pageIndex)
        {
            return (pageIndex <= TotalPages && pageIndex >= 1 && pageIndex == CurrentPageIndex);
        }
        #endregion

        public BasePagingViewModel()
        {
            Items = new List<T>();
        }
    }
}