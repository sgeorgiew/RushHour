﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using RushHour.Data.Entities;
using System.Web.Mvc;

namespace RushHour.ViewModels
{
    public class AppointmentEditViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Start date and time is required")]
        [Display(Name = "Start Date")]
        public DateTime? StartDateTime { get; set; }

        public DateTime? EndDateTime { get; set; }

        [Display(Name = "Activities")]
        public List<Activity> Activities { get; set; }

        public List<int> ChosenActivities { get; set; }

        #region Constructors
        public AppointmentEditViewModel()
        {
            Activities = new List<Activity>();
        }

        public AppointmentEditViewModel(Appointment appointment)
        {
            this.Id = appointment.Id;
            this.StartDateTime = appointment.StartDateTime;
            this.EndDateTime = appointment.EndDateTime;
            this.ChosenActivities = appointment.Activities.Select(x => x.Id).ToList();
        }
        #endregion
    }
}