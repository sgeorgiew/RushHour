﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RushHour.Data.Entities;

namespace RushHour.ViewModels
{
    public class ActivityPagingViewModel : BasePagingViewModel<ActivityViewModel>
    {
        public ActivityPagingViewModel(List<Activity> list, int pageIndex, int recordsCount) : base()
        {
            list.ForEach(item => Items.Add(new ActivityViewModel(item)));

            TotalItems = recordsCount;
            TotalPages = ((recordsCount - 1) / ItemsPerPage) + 1;
            CurrentPageIndex = pageIndex;
        }
    }
}