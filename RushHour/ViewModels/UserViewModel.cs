﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RushHour.Data.Entities;
using System.ComponentModel.DataAnnotations;

namespace RushHour.ViewModels
{
    public class UserViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Please enter a valid email address")]
        [Display(Name = "Email Address")]
        public string Email { get; set; }
        
        [StringLength(24, MinimumLength = 6, ErrorMessage = "Your password must be between {2} and {1} symbols")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "The name is required")]
        [StringLength(30, MinimumLength = 4, ErrorMessage = "The name must be between {2} and {1} letters")]
        [RegularExpression("([a-zA-Zа-яА-Я]+\\s?)+", ErrorMessage = "The name can contain only letters and 1 space between the words")]
        [Display(Name = "Full Name")]
        public string Name { get; set; }

        [RegularExpression(@"\(?\d{3}\)?-? *\d{3}-? *-?\d{4}", ErrorMessage = "Please enter a valid phone number")]
        [Display(Name = "Phone Number")]
        public string Phone { get; set; }

        #region Constructors
        public UserViewModel()
        { }

        public UserViewModel(User dbUser)
        {
            this.Id = dbUser.Id;
            this.Email = dbUser.Email;
            this.Name = dbUser.Name;
            this.Phone = dbUser.Phone;
        }
        #endregion
    }
}