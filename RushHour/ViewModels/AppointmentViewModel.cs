﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using RushHour.Data.Entities;

namespace RushHour.ViewModels
{
    public class AppointmentViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Start Date")]
        public DateTime StartDateTime { get; set; }

        [Display(Name = "End Date")]
        public DateTime EndDateTime { get; set; }

        [Display(Name = "Total Price")]
        public decimal TotalPrice { get; set; }

        [Display(Name = "Created by")]
        public string CreatedBy { get; set; }
        
        public bool IsCancelled { get; set; }

        #region Constructors
        public AppointmentViewModel()
        { }

        public AppointmentViewModel(Appointment appointment)
        {
            this.Id = appointment.Id;
            this.StartDateTime = appointment.StartDateTime;
            this.EndDateTime = appointment.EndDateTime;
            this.TotalPrice = appointment.Activities.Sum(x => x.Price);
            this.CreatedBy = appointment.User.Name;
            this.IsCancelled = appointment.IsCancelled;
        }
        #endregion
    }
}