﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RushHour.Data.Entities;

namespace RushHour.ViewModels
{
    public class UserProfileViewModel
    {
        public int Id { get; set; }

        public string Email { get; set; }
        
        public string Name { get; set; }
        
        public string Phone { get; set; }

        public bool IsAdministrator { get; set; }

        #region Constructors
        public UserProfileViewModel()
        { }

        public UserProfileViewModel(User dbUser)
        {
            this.Id = dbUser.Id;
            this.Email = dbUser.Email;
            this.Name = dbUser.Name;
            this.Phone = dbUser.Phone;
            this.IsAdministrator = dbUser.IsAdministrator;
        }
        #endregion
    }
}