﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RushHour.Helpers;
using RushHour.ViewModels;
using RushHour.Data.Entities;
using RushHour.Services;

namespace RushHour.Controllers
{
    [CustomAuthorize]
    public class ActivityController : Controller
    {
        private ActivityService activityService;

        public ActivityController()
        {
            this.activityService = new ActivityService(new ModelStateWrapper(this.ModelState));
        }

        // GET: Activity
        public ActionResult Index(int page = 1)
        {
            ViewBag.SuccessMessage = TempData["Message"];
            ViewBag.ErrorMessage = TempData["ErrorMessage"];
            
            var records = activityService.GetAll();

            // Check for negative or zero page number
            if (page < 1)
            {
                page = 1;
            }

            // Convert the IQueryable list to list (i.e. retrieve the data from the database)
            var list = records
                .OrderByDescending(x => x.Id)
                .Skip((page - 1) * ActivityPagingViewModel.ItemsPerPage)
                .Take(ActivityPagingViewModel.ItemsPerPage)
                .ToList();

            int allRecordsCount = records.Count();

            // Create the view model
            ActivityPagingViewModel model = new ActivityPagingViewModel(list, page, allRecordsCount);

            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id = 0)
        {
            ActivityViewModel model = new ActivityViewModel();
            Activity dbActivity = activityService.GetById(id);

            if (dbActivity != null)
            {
                model = new ActivityViewModel(dbActivity);
            }

            return View(model);
        }

        [HttpPost]
        [ActionName("Edit")]
        public ActionResult EditPost(ActivityViewModel viewModel)
        {
            Activity dbActivity = activityService.GetById(viewModel.Id);

            if (dbActivity == null)
            {
                dbActivity = new Activity();
            }

            dbActivity.Name = viewModel.Name;
            dbActivity.Duration = viewModel.Duration;
            dbActivity.Price = viewModel.Price;

            if (!activityService.AddOrUpdate(dbActivity))
            {
                return View();
            }
            
            TempData["Message"] = "The activity was saved successfully!";

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id = 0)
        {
            if (activityService.DeleteById(id))
            {
                TempData["Message"] = "The activity was successfully deleted!";
            }
            else
            {
                TempData["ErrorMessage"] = "No activity found!";
            }

            return RedirectToAction("Index");
        }
    }
}