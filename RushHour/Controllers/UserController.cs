﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RushHour.ViewModels;
using RushHour.Helpers;
using RushHour.Data.Entities;
using RushHour.Data.Repositories;
using RushHour.Services;

namespace RushHour.Controllers
{
    public class UserController : Controller
    {
        private UserService userService;

        public UserController()
        {
            this.userService = new UserService(new ModelStateWrapper(this.ModelState));
        }

        // GET: User
        [CustomAuthorize(AllowAccessToUser = true)]
        public ActionResult Index()
        {
            int loggedUserId = AuthenticationService.Current.UserId;
            User user = userService.GetById(loggedUserId);
            UserProfileViewModel model = new UserProfileViewModel(user);

            return View(model);
        }

        [HttpGet]
        public ActionResult Login()
        {
            if (AuthenticationService.Current.IsAuthenticated)
            {
                return RedirectToAction("Index");
            }

            return View();
        }

        [HttpPost]
        [ActionName("Login")]
        public ActionResult LoginPost(LoginViewModel viewModel)
        {
            User dbUser = userService.GetByEmailAndPassword(viewModel.Email, viewModel.Password);

            if (!userService.ValidateDbUser(dbUser))
            {
                ModelState.AddModelError("", "Wrong email address or password!");
                return View();
            }

            AuthenticationService.Current.Authenticate(dbUser.Id, dbUser.Email, dbUser.IsAdministrator);
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult Register()
        {
            if (AuthenticationService.Current.IsAuthenticated)
            {
                return RedirectToAction("Index");
            }

            ViewBag.SuccessMessage = TempData["Message"];

            return View();
        }

        [HttpPost]
        [ActionName("Register")]
        public ActionResult RegisterPost(RegisterViewModel viewModel)
        {
            User user = new User()
            {
                Email = viewModel.Email,
                Password = viewModel.Password,
                Name = viewModel.Name.Trim(' '),
                Phone = viewModel.Phone
            };

            if (!userService.AddOrUpdate(user))
            {
                TempData["ErrorMessage"] = "Ooops something went wrong";
                return View(viewModel);
            }

            TempData["Message"] = "You are successfully registered! Now you are able to log in.";

            return RedirectToAction("Register");
        }

        [HttpGet]
        [CustomAuthorize(AllowAccessToUser = true)]
        public ActionResult Edit(int id = 0)
        {
            ViewBag.SuccessMessage = TempData["Message"];
            ViewBag.ErrorMessage = TempData["ErrorMessage"];

            int loggedUserId = AuthenticationService.Current.UserId;

            if (loggedUserId != id && !AuthenticationService.Current.IsAdministrator)
            {
                return RedirectToAction("Edit", new { id = loggedUserId });
            }

            UserViewModel model = new UserViewModel();
            
            User dbUser = userService.GetById(id);

            if (dbUser != null)
            {
                model = new UserViewModel(dbUser);
            }

            return View(model);
        }

        [HttpPost]
        [CustomAuthorize(AllowAccessToUser = true)]
        [ActionName("Edit")]
        public ActionResult EditPost(UserViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    User dbUser = userService.GetById(viewModel.Id);

                    if (dbUser == null)
                    {
                        dbUser = new User();
                    }

                    dbUser.Email = viewModel.Email;
                    dbUser.Name = viewModel.Name.Trim(' ');
                    dbUser.Phone = viewModel.Phone;

                    if (!string.IsNullOrEmpty(viewModel.Password))
                    {
                        dbUser.Password = viewModel.Password;
                    }

                    if (userService.AddOrUpdate(dbUser))
                    {
                        ViewBag.SuccessMessage = "The profile is updated successfully!";
                    }
                }
                catch (Exception)
                {
                    ViewBag.ErrorMessage = "Something went wrong! Please try again.";
                }
            }

            return View();
        }

        [CustomAuthorize(AllowAccessToUser = true)]
        public ActionResult Logout()
        {
            AuthenticationService.Current.Logout();
            ViewBag.Message = "You have successfully logged out. You will be redirected to the homepage after 3 seconds!";
            Response.AddHeader("REFRESH", "3;URL=" + Url.Action("Index", "Home") + "");

            return View();
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult All(int page = 1)
        {
            ViewBag.SuccessMessage = TempData["Message"];
            ViewBag.ErrorMessage = TempData["ErrorMessage"];
            
            var records = userService.GetAll();

            // Check for negative or zero page number
            if (page < 1)
            {
                page = 1;
            }

            // Convert the IQueryable list to list (i.e. retrieve the data from the database)
            var list = records
                .OrderByDescending(x => x.Id)
                .Skip((page - 1) * AppointmentPagingViewModel.ItemsPerPage)
                .Take(AppointmentPagingViewModel.ItemsPerPage)
                .ToList();

            int allRecordsCount = records.Count();

            // Create the view model
            UserPagingViewModel model = new UserPagingViewModel(list, page, allRecordsCount);

            return View(model);
        }
        
        [CustomAuthorize]
        public ActionResult Delete(int id = 0)
        {
            if (userService.DeleteById(id))
            {
                TempData["Message"] = "The user was successfully deleted!";
            }
            else
            {
                TempData["ErrorMessage"] = "No user found!";
            }

            return RedirectToAction("All");
        }

        [CustomAuthorize]
        public ActionResult MakeAdmin(int id)
        {
            User user = userService.GetById(id);

            if (user != null)
            {
                user.IsAdministrator = !user.IsAdministrator;
                userService.AddOrUpdate(user);
            }

            return RedirectToAction("All");
        }
    }
}