﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RushHour.ViewModels;
using RushHour.Helpers;
using RushHour.Data.Entities;
using RushHour.Services;

namespace RushHour.Controllers
{
    [CustomAuthorize(AllowAccessToUser = true)]
    public class AppointmentController : Controller
    {
        private AppointmentService appointmentService;

        public AppointmentController()
        {
            this.appointmentService = new AppointmentService(new ModelStateWrapper(this.ModelState));
        }

        // GET: Appointment
        public ActionResult Index(int page = 1)
        {
            ViewBag.SuccessMessage = TempData["Message"];
            ViewBag.ErrorMessage = TempData["ErrorMessage"];

            int loggedUserId = AuthenticationService.Current.UserId;
            
            var records = appointmentService.GetAll(u => u.UserId == loggedUserId);

            // Check for negative or zero page number
            if (page < 1)
            {
                page = 1;
            }

            // Convert the IQueryable list to list (i.e. retrieve the data from the database)
            var list = records
                .OrderByDescending(x => x.Id)
                .Skip((page - 1) * AppointmentPagingViewModel.ItemsPerPage)
                .Take(AppointmentPagingViewModel.ItemsPerPage)
                .ToList();

            int allRecordsCount = records.Count();

            // Create the view model
            AppointmentPagingViewModel model = new AppointmentPagingViewModel(list, page, allRecordsCount);

            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id = 0)
        {
            Appointment dbAppointment = appointmentService.GetById(id);

            int loggedUserId = AuthenticationService.Current.UserId;
            if (!appointmentService.HaveAccess(dbAppointment, loggedUserId))
            {
                TempData["ErrorMessage"] = "The appointment was not found!";
                return RedirectToAction("Index");
            }

            AppointmentEditViewModel model = new AppointmentEditViewModel();

            if (dbAppointment != null)
            {
                model = new AppointmentEditViewModel(dbAppointment);
            }
            
            List<Activity> activities = appointmentService.GetAllActivities().ToList();
            model.Activities = activities;

            return View(model);
        }

        [HttpPost]
        [ActionName("Edit")]
        public ActionResult EditPost(AppointmentEditViewModel viewModel)
        {
            Appointment dbAppointment = appointmentService.GetById(viewModel.Id);

            if (dbAppointment == null)
            {
                dbAppointment = new Appointment()
                {
                    Activities = new List<Activity>()
                };
            }
            else
            {
                dbAppointment.Activities.Clear();
            }

            viewModel.ChosenActivities?.ForEach(x => { dbAppointment.Activities.Add(appointmentService.GetActivityById(x)); });

            var duration = dbAppointment.Activities.Sum(x => x.Duration);

            dbAppointment.StartDateTime = viewModel.StartDateTime ?? DateTime.MinValue;
            dbAppointment.EndDateTime = dbAppointment.StartDateTime + TimeSpan.FromMinutes(duration);
            dbAppointment.UserId = AuthenticationService.Current.UserId;

            if (!appointmentService.AddOrUpdate(dbAppointment))
            {
                List<Activity> activities = appointmentService.GetAllActivities().ToList();
                viewModel.Activities = activities;

                return View(viewModel);
            }
            
            TempData["Message"] = "The appointment was saved successfully!";

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Details(int id = 0)
        {
            if (id <= 0)
            {
                return RedirectToAction("Index");
            }
            
            Appointment dbAppointment = appointmentService.GetById(id);

            int loggedUserId = AuthenticationService.Current.UserId;
            if (!appointmentService.HaveAccess(dbAppointment, loggedUserId) && !AuthenticationService.Current.IsAdministrator)
            {
                TempData["ErrorMessage"] = "The appointment was not found!";
                return RedirectToAction("Index");
            }

            AppointmentDetailsViewModel model = new AppointmentDetailsViewModel(dbAppointment);

            return View(model);
        }

        public ActionResult Delete(int id = 0)
        {
            if (id <= 0)
            {
                return RedirectToAction("Index");
            }

            int loggedUserId = AuthenticationService.Current.UserId;
            Appointment dbAppointment = appointmentService.GetById(id);

            if (appointmentService.HaveAccess(dbAppointment, loggedUserId) || 
                AuthenticationService.Current.IsAdministrator)
            {
                if (appointmentService.DeleteById(id))
                {
                    TempData["Message"] = "The appointment was successfully deleted!";
                }
                else
                {
                    TempData["ErrorMessage"] = "No appointment found!";
                }
            }
            else
            {
                TempData["ErrorMessage"] = "This appointment is not yours!";
            }

            if (Request.UrlReferrer != null)
                return Redirect(Request.UrlReferrer.ToString());

            return RedirectToAction("Index");
        }

        public ActionResult Cancel(int id = 0)
        {
            if (id <= 0)
            {
                return RedirectToAction("Index");
            }

            Appointment dbAppointment = appointmentService.GetById(id);

            int loggedUserId = AuthenticationService.Current.UserId;

            if (appointmentService.HaveAccess(dbAppointment, loggedUserId) ||
                AuthenticationService.Current.IsAdministrator)
            {
                dbAppointment.IsCancelled = true;

                if (appointmentService.AddOrUpdate(dbAppointment))
                {
                    TempData["Message"] = "The appointment was successfully cancelled!";
                }
                else
                {
                    TempData["ErrorMessage"] = "No appointment found!";
                }
            }
            else
            {
                TempData["ErrorMessage"] = "This appointment is not yours!";
            }

            if (Request.UrlReferrer != null)
                return Redirect(Request.UrlReferrer.ToString());

            return RedirectToAction("Index");
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult All(int page = 1)
        {
            ViewBag.SuccessMessage = TempData["Message"];
            ViewBag.ErrorMessage = TempData["ErrorMessage"];
            
            var records = appointmentService.GetAll();

            // Check for negative or zero page number
            if (page < 1)
            {
                page = 1;
            }

            // Convert the IQueryable list to list (i.e. retrieve the data from the database)
            var list = records
                .OrderByDescending(x => x.Id)
                .Skip((page - 1) * AppointmentPagingViewModel.ItemsPerPage)
                .Take(AppointmentPagingViewModel.ItemsPerPage)
                .ToList();

            int allRecordsCount = records.Count();

            // Create the view model
            AppointmentPagingViewModel model = new AppointmentPagingViewModel(list, page, allRecordsCount);

            return View(model);
        }
    }
}